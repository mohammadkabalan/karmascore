<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use App\Models\Image ;

class UserFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {

        return [
            'id' =>  $this->faker->unique()->numerify('#######'),
            'username' => $this->faker->unique()->name(),
            'karma_score' => $this->faker->numerify('########'),
            'image_id'=> Image::inRandomOrder()->first()->id ,
        ];
    }

}
