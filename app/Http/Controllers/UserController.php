<?php

namespace App\Http\Controllers;

use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use mysql_xdevapi\Exception;

class UserController extends Controller
{

    /*****
     * @param $id
     * @return array|string
     */
    public function karmaScore($id)
    {
        try {
            $result = [];
            $max_score_in_table = DB::table('users')->max('karma_score');
            $min_score_in_Table = DB::table('users')->min('karma_score');
            $user = DB::table('users as u')
                ->join('images', 'u.image_id', '=', 'images.id')
                ->select('u.id', 'u.username', 'u.karma_score', 'images.url')
                ->where('u.id', $id)
                ->first();
            $userScore = $user->karma_score;

            if ($userScore != $max_score_in_table && $userScore != $min_score_in_Table) {
                $max = DB::table('users as u')
                    ->join('images', 'u.image_id', '=', 'images.id')
                    ->select('u.id', 'u.username', 'u.karma_score', 'images.url')
                    ->where('u.karma_score', '>', $userScore)
                    ->orderBy('u.karma_score', 'ASC')
                    ->limit(2)
                    ->get();

                $min = DB::table('users as u')
                    ->join('images', 'u.image_id', '=', 'images.id')
                    ->select('u.id', 'u.username', 'u.karma_score', 'images.url')
                    ->where('u.karma_score', '<', $userScore)
                    ->orderBy('u.karma_score', 'DESC')
                    ->limit(2)
                    ->get();


                $max = $this->swapTwoValues($max[0], $max[1]);
                $result[0] = $max[1];
                $result[1] = $max[0];
                $result[2] = $user;
                $result[3] = $min[0];
                $result[4] = $min[1];

                return $result;
            } elseif ($userScore == $max_score_in_table) {
                $max = DB::table('users as u')
                    ->join('images', 'u.image_id', '=', 'images.id')
                    ->select('u.id', 'u.username', 'u.karma_score', 'images.url')
                    ->where('u.karma_score', '!=', $max_score_in_table)
                    ->orderBy('u.karma_score', 'DESC')
                    ->limit(4)
                    ->get();
                $result[0] = $user;
                $result[1] = $max[0];
                $result[2] = $max[1];
                $result[3] = $max[2];
                $result[4] = $max[3];

                return $result;
            }
            else
            {
                $min = DB::table('users as u')
                    ->join('images', 'u.image_id', '=', 'images.id')
                    ->select('u.id', 'u.username', 'u.karma_score', 'images.url')
                    ->where('u.karma_score', '!=', $min_score_in_Table)
                    ->orderBy('u.karma_score', 'ASC')
                    ->limit(4)
                    ->get();
                $result[0] = $user;
                $result[1] = $min[0];
                $result[2] = $min[1];
                $result[3] = $min[2];
                $result[4] = $min[3];
                return $result;
            }
        }
        catch(\Throwable $e){
            return $e->getMessage() ;
        }
    }

    /****
     * @param $id
     * @param $numberHigher
     * @param $numberLower
     * @return array|\Exception|\Throwable
     */
    public function karmaScoreDynamic($id,$numberHigher,$numberLower)
    {
        try
        {
            $result = [] ;
            $user = DB::table('users as u')
                ->join('images', 'u.image_id', '=', 'images.id')
                ->select('u.id', 'u.username', 'u.karma_score', 'images.url')
                ->where('u.id', $id)
                ->first();
            $userScore = $user->karma_score;

            $max = DB::table('users as u')
                ->join('images', 'u.image_id', '=', 'images.id')
                ->select('u.id', 'u.username', 'u.karma_score', 'images.url')
                ->where('u.karma_score', '>', $userScore)
                ->orderBy('u.karma_score', 'ASC')
                ->limit($numberHigher)
                ->get();

            $min = DB::table('users as u')
                ->join('images', 'u.image_id', '=', 'images.id')
                ->select('u.id', 'u.username', 'u.karma_score', 'images.url')
                ->where('u.karma_score', '<', $userScore)
                ->orderBy('u.karma_score', 'DESC')
                ->limit($numberLower)
                ->get();

            $max2 = $max->toArray() ;
            $min2 = $min->toArray() ;


            //sort the max array by score
            usort($max2, function ($a, $b) {
                     return $a->karma_score < $b->karma_score ;
                });

           $result    = $max2 ;
           $result [] = $user ;
           $final_res = array_merge($result ,$min2) ;


             return $final_res ;
        }
        catch(\Throwable $e)
        {
          return $e ;
        }
    }
    /***
     * @param $x
     * @param $y
     * @return array
     */
    private function swapTwoValues($x , $y)
    {
        $z = [] ;
        $value1 = (object)$x ;
        $value2 = (object)$y ;
        if($value1 > $value2)
        {
            $z[0] = $value1 ;
            $z[1] = $value2 ;
        }
        else
        {
            $z[0] = $value2 ;
            $z[1] = $value1 ;
        }
        return $z ;
    }

}
