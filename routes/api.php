<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['prefix'=>'v2'],function() {
    Route::get('user/{id}/karma-position', [UserController::class, 'karmaScore']);
});

Route::group(['prefix'=>'v3'],function() {
    Route::get('user/{id}/{higher}/{lower}/karma-position', [UserController::class, 'karmaScoreDynamic']);
});
